from send import create_app, db

app = create_app()

if __name__ == '__main__':
    app.run(debug=True)

app.app_context().push()