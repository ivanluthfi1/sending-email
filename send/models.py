from datetime import datetime
from send import db
from flask import current_app

class Email(db.Model):
    id         = db.Column(db.Integer, primary_key=True)
    subject    = db.Column(db.String(128), nullable=False)
    recipient1 = db.Column(db.String(120), nullable=False)
    recipient2 = db.Column(db.String(120))
    recipient3 = db.Column(db.String(120))
    recipient4 = db.Column(db.String(120))
    content    = db.Column(db.Text, nullable=False)
    datetime   = db.Column(db.DateTime, nullable=False)

    def __repr__(self):
        return f"Email('{self.subject}', '{self.recipient1}', '{self.recipient2}', '{self.recipient3}', '{self.recipient4}', '{self.content}', '{self.datetime}')"