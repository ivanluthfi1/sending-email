from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_mail import Mail, Message
from send.config import Config
from send.flask_celery import make_celery

db = SQLAlchemy()
mail = Mail()

def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(Config)
    app.config.update(
        CELERY_BROKER_URL='redis://localhost//',
        CELERY_RESULT_BACKEND='redis://localhost//'
    )

    celery = make_celery(app)
    db.init_app(app)
    mail.init_app(app)

    from send.emails.routes import emails
    
    app.register_blueprint(emails)

    return app
