from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, TextAreaField, IntegerField
from wtforms.validators import DataRequired

class EmailForm(FlaskForm):
    subject = StringField('Subject', validators=[DataRequired()])
    recipient1 = StringField('Recipient 1', validators=[DataRequired()])
    recipient2 = StringField('Recipient 2')
    recipient3 = StringField('Recipient 3')
    recipient4 = StringField('Recipient 4')
    content    = TextAreaField('Email Content', validators=[DataRequired()])
    delay      = IntegerField('Delay time from now (in second)', validators=[DataRequired()])
    submit     = SubmitField('Send')