from flask import (render_template, url_for, flash,
                    redirect, request, abort, Blueprint)
from send import db, Message, mail, create_app
from datetime import datetime, timedelta
from send.models import Email
from send.emails.forms import EmailForm
from send.flask_celery import make_celery
from celery import Celery, Task

emails = Blueprint('emails', __name__)

@emails.route("/save_emails", methods=['GET','POST'])
def send():
    form = EmailForm()
    if form.validate_on_submit():
        subject = form.subject.data
        recipient1 = form.recipient1.data
        recipient2 = form.recipient2.data
        recipient3 = form.recipient3.data
        recipient4 = form.recipient4.data
        content = form.content.data
        email = Email(subject=subject, recipient1=recipient1, recipient2=recipient2, recipient3=recipient3, recipient4=recipient4, content=content, datetime=datetime.now() + timedelta(seconds=int(form.delay.data)))
        db.session.add(email)
        db.session.commit()
        msg = Message(subject=subject, recipients=[recipient1, recipient2, recipient3, recipient4])
        msg.body = content
        mail.send(msg)
        # send.apply_async(subject, recipient1, recipient2, recipient3, recipient4, content, countdown=int(form.delay.data))
        flash('Email has been sent!', 'success')
        return redirect(url_for('emails.send'))
    return render_template('send.html', title='New Email', form=form, legend='New Email')

app = create_app()
celery = make_celery(app)

# @celery.task(name='celery_mail_send')
# def send(subject, recipient1, recipient2, recipient3, recipient4, content):
#     msg = Message(subject=subject, recipients=[recipient1, recipient2, recipient3, recipient4])
#     msg.body = content
#     mail.send(msg)