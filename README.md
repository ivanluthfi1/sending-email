Instructions:

Here is an instruction of how to run this application.

1. clone this project into a folder in your local computer
2. run this command in terminal (on the folder path): python run.py
3. look at the console, there is an info of where is the url of this application: Running on http://127.0.0.1:5000/ 
4. open that url on your browser, dont forget to add "/saveemails" as it's its routes
5. so you have to access: Running on http://127.0.0.1:5000/save_emails
6. input the subject, email recipients, and contents, then send

notes: the scheduling task isn't work well, so for now it's can't be used